from django.apps import AppConfig


class DjangoFilesyncConfig(AppConfig):
    name = 'django_filesync'
